package se325.lab04.concert.services;

import se325.lab04.concert.domain.Concert;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/concerts")
public class DatabaseResource {
    private static long id = 0;

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, "application/java-serialization"})
    public Response retrieve(@PathParam("id") long id){
        EntityManager entityManager = PersistenceManager.instance().createEntityManager();
        entityManager.getTransaction().begin();
        Concert result = entityManager.find(Concert.class, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        if(result != null)
            return Response.ok(result).build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    @POST
    @Consumes({MediaType.APPLICATION_JSON, "application/java-serialization"})
    public Response create(Concert c){
        EntityManager entityManager = PersistenceManager.instance().createEntityManager();
        entityManager.getTransaction().begin();
        Concert result = new Concert(id++,c.getTitle(),c.getDate(),c.getPerformer());
        entityManager.persist(result);
        entityManager.getTransaction().commit();
        entityManager.close();
        return Response.created(URI.create("/concerts/" + result.getId())).build();
    }
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, "application/java-serialization"})
    public Response update(Concert c) {
        EntityManager entityManager = PersistenceManager.instance().createEntityManager();
        entityManager.getTransaction().begin();
        Concert result = entityManager.find(Concert.class,c.getId());
        if(result != null)
            entityManager.merge(c);
        entityManager.getTransaction().commit();
        entityManager.close();
        if(result != null)
            return Response.noContent().build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") long id){
        EntityManager entityManager = PersistenceManager.instance().createEntityManager();
        entityManager.getTransaction().begin();
        Concert result = entityManager.find(Concert.class,id);
        entityManager.remove(result);
        entityManager.getTransaction().commit();
        entityManager.close();
        if(result != null)
            return Response.noContent().build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    @DELETE
    public Response deleteAll(){
        EntityManager entityManager = PersistenceManager.instance().createEntityManager();
        entityManager.getTransaction().begin();
        TypedQuery<Concert> concertQuery = entityManager.createQuery("select c from Concert c", Concert.class);
        for(Concert match:concertQuery.getResultList())
            entityManager.remove(match);
        entityManager.getTransaction().commit();
        entityManager.close();
        return Response.noContent().build();
    }
}
