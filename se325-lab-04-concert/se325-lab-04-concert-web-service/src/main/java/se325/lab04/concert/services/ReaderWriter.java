package se325.lab04.concert.services;

import org.apache.commons.lang3.SerializationUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Produces("application/java-serialization")
@Consumes("application/java-serialization")
public class ReaderWriter implements MessageBodyReader<Serializable>, MessageBodyWriter<Serializable> {
    @Override
    public boolean isReadable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return mediaType.isCompatible(MediaType.valueOf("application/java-serialization"));
    }

    @Override
    public Serializable readFrom(Class<Serializable> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> multivaluedMap, InputStream inputStream) throws IOException, WebApplicationException {
        return (Serializable) SerializationUtils.deserialize(inputStream);
    }

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return mediaType.isCompatible(MediaType.valueOf("application/java-serialization"));
    }

    @Override
    public void writeTo(Serializable serializable, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        SerializationUtils.serialize(serializable, outputStream);
    }
}
