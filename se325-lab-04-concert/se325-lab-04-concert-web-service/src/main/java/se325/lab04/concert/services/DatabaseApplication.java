package se325.lab04.concert.services;

import org.h2.engine.Database;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class DatabaseApplication extends Application {
    private final Set<Object> singletons = new HashSet<>();
    private final Set<Class<?>> classes = new HashSet<>();

    public DatabaseApplication() {
        singletons.add(PersistenceManager.instance());
        classes.add(DatabaseResource.class);
        classes.add(ReaderWriter.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
